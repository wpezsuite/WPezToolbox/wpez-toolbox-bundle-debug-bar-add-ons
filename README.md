## WPezToolbox - Bundle: Debug Bar Add-Ons

__A WPezToolbox bundle of third-party add-ons for the famous Debug Bar plugin.__

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Overview

See the README here: https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-loader


### This Bundle Includes

TODO


### Helpful Links

- https://gitlab.com/wpezsuite/WPezToolbox

- https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-loader

- https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-dev


### TODO 



### CHANGE LOG

- v0.0.0 - 17 November 2019
   
   Proof of Concept

