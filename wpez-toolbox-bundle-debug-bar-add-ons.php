<?php
/*
Plugin Name: WPezToolbox - Bundle: Debug Bar Add-Ons
Plugin URI: https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-debug-bar-add-ons
Description: DBB+ - A WPezToolbox bundle of third-party add-ons for the famous Debug Bar plugin.
Version: 0.0.0
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: wpez_tbox
*/

namespace WPezToolboxBundleDebugBarAddOns;

add_filter( 'WPezToolboxLoader\plugins', __NAMESPACE__ . '\plugins' );


function plugins( $arr_in = [] ) {

	$str_bundle           = 'DBB+';
	$str_prefix_separator = ' | ';
	$str_prefix           = $str_bundle . $str_prefix_separator;

	$arr = [


		// =============================================
		//
		// ===== DB+ - Debug Bar Add-ons =====
		// https://github.com/johnbillion/query-monitor/wiki/Query-Monitor-Add-on-Plugin
		//
		// =============================================

		'debug-bar-actions-and-filters-addon' =>
			[
				'name'     => $str_prefix . 'Debug Bar: Actions and Filters',
				'slug'     => 'debug-bar-actions-and-filters-addon',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'Subharanjan',
						'url_by'  => 'http://subharanjan.com/',
						'desc'    => 'Displays hooks(Actions and Filters) attached to the current request. Actions tab displays the actions hooked to current request.',
						'url_img' => 'https://ps.w.org/debug-bar-actions-and-filters-addon/assets/icon-128x128.png?rev=972076'
					],
					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/debug-bar-actions-and-filters-addon/',
						'url_repo'   => 'https://github.com/subharanjanm/debug-bar-actions-and-filters-addon',
						'url_tw'     => 'https://twitter.com/subharanjanm'
					]
				]
			],

		'debug-bar-cache-lookup' => [
			'name'     => $str_prefix . 'Debug Bar: Cache Lookup',
			'slug'     => 'debug-bar-cache-lookup',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Allan Collins',
					'url_by'  => 'https://allancollins.net/',
					'desc'    => 'Are you running an object cache like memcached? Want to see if cache is working?',
					'url_img' => 'https://ps.w.org/debug-bar-cache-lookup/assets/icon-128x128.jpg?rev=1110414'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/debug-bar-cache-lookup/',
				]
			]
		],


		'debug-bar-constants' => [
			'name'     => $str_prefix . 'Debug Bar: Constants',
			'slug'     => 'debug-bar-constants',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Juliette Reinders Folmer',
					'url_by'  => 'http://www.adviesenzo.nl/',
					'desc'    => 'Adds three new panels to the Debug Bar that display the defined constants available to you as a developer for the current request: WP Constants, WP Class Constants, PHP Constants.',
					'url_img' => 'https://ps.w.org/customizer-export-import/assets/icon-128x128.jpg?rev=1049984'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/customizer-export-import/',
				]
			]
		],

		'debug-bar-cron' => [
			'name'     => $str_prefix . 'Debug Bar: Cron',
			'slug'     => 'debug-bar-cron',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Zack Tollman, Helen Hou-Sandi',
					'url_by'  => 'https://www.tollmanz.com/',
					'desc'    => 'Adds information about WP scheduled events to a new panel in the Debug Bar.',
					'url_img' => 'https://avatars2.githubusercontent.com/u/921795?s=400&v=4'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/debug-bar-cron/',
					'url_repo'   => 'https://github.com/tollmanz/debug-bar-cron'
				]
			]
		],

		'debug-bar-extender' => [
			'name'     => $str_prefix . 'Debug Bar: Extender',
			'slug'     => 'debug-bar-extender',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Thorsten Ott, Automattic',
					'url_by'  => 'https://hitchhackerguide.com/',
					'desc'    => 'Adds more features to the Debug Bar plugin, and is mainly aimed at developers who like to debug their code or want to measure runtimes to find glitches in their code.',
					'url_img' => 'https://secure.gravatar.com/avatar/9e8f7fd87fd2163010f98280f3e45a35?s=100&d=mm&r=g'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/debug-bar-extender/',
				]
			]
		],

		'debug-bar-elasticpress' => [
			'name'     => $str_prefix . 'Debug Bar: ElasticPress',
			'slug'     => 'debug-bar-elasticpress',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => '10up',
					'by_alt'  => 'WP.org Profile',
					'url_by'  => 'https://profiles.wordpress.org/10up/',
					'desc'    => 'Adds an ElasticPress panel to the Debug Bar plugin. Allows you to examine every ElasticPress query running on any given request.',
					'url_img' => 'https://secure.gravatar.com/avatar/78f1d9ceab2dbc8f92f875ec1637e2f1?s=100&d=mm&r=g'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/debug-bar-elasticpress/',
					'url_repo' => 'https://github.com/10up/debug-bar-elasticpress',
					'url_site' => 'https://10up.com/'
				]
			]
		],

		'log-viewer' => [
			'name'     => $str_prefix . 'Debug Bar: Log Viewer',
			'slug'     => 'log-viewer',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Markus Fischbacher',
					'by_alt'  => 'WP.org Profile',
					'url_by'  => 'https://profiles.wordpress.org/mfisc/',
					'desc'    => '// Debug Bar: Log Viewer - This plugin provides an easy way to view any *.log files directly in admin panel. Also you can perform simple actions like empty the file or deleting it.',
					'url_img' => 'https://secure.gravatar.com/avatar/1620f8c5c2d660556ed6827fe2a2b5ca?s=100&d=mm&r=g'
				],
				'resources' => [
					'url_wp_org' => '// https://wordpress.org/plugins/log-viewer/',
				]
			]
		],

		'debug-media' => [
			'name'     => $str_prefix . 'Debug Bar: Media',
			'slug'     => 'debug-media',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Joe McGill',
					'url_by'  => 'https://joemcgill.net/',
					'desc'    => 'Adds a Media panel to the Debug Bar.',
					'url_img' => 'https://secure.gravatar.com/avatar/7cef1c9108207ec24db7a40f142db676?s=100&d=mm&r=g'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/debug-media/',
				]
			]
		],

		'debug-bar-plugin-activation' => [
			'name'     => $str_prefix . 'Debug Bar: Plugin Activation',
			'slug'     => 'debug-bar-plugin-activation',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Juliette Reinders Folmer',
					'by_alt'  => 'WP.org Profile',
					'url_by'  => 'https://profiles.wordpress.org/website-redirect/jrf',
					'desc'    => 'Adds a new panel to the Debug Bar which displays the output generated during plugin activation, deactivation and uninstall.',
					'url_img' => 'https://secure.gravatar.com/avatar/cbbac3e529102364dc3b026af3cc2988?s=100&d=mm&r=g'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/debug-bar-plugin-activation/',
				]
			]
		],

		'debug-bar-post-meta' => [
			'name'     => $str_prefix . 'Debug Bar: Post Meta',
			'slug'     => 'debug-bar-post-meta',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Taylor Dewey',
					'by_alt'  => 'WP.org Profile',
					'url_by'  => 'https://profiles.wordpress.org/taylorde/',
					'desc'    => 'Adds a pane to the debug bar with information about post meta for the current post.',
					'url_img' => 'https://secure.gravatar.com/avatar/7a31d1b50487315952c82fb43123f6e7?s=100&d=mm&r=g'
				],
				'resources' => [
					'url_wp_org' => '// https://wordpress.org/plugins/tdd-debug-bar-post-meta/',
					'url_repo'   => 'https://github.com/tddewey/tdd-debug-bar-post-meta'
				]
			]
		],



		'debug-bar-post-types' => [
			'name'     => $str_prefix . 'Debug Bar: Post Types',
			'slug'     => 'debug-bar-post-types',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Juliette Reinders Folmer',
					'by_alt'  => 'WP.org Profile',
					'url_by'  => 'https://profiles.wordpress.org/website-redirect/jrf',
					'desc'    => 'Adds a new panel to the Debug Bar that displays detailed information about the registered post types for your site.',
					'url_img' => 'https://ps.w.org/debug-bar-post-types/assets/icon-128x128.png?rev=981219'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/debug-bar-post-types/',
				]
			]
		],

		'debug-bar-rewrite-rules' => [
			'name'     => $str_prefix . 'Debug Bar: Rewrite Rules',
			'slug'     => 'debug-bar-rewrite-rules',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Oleg Butuzov',
					'by_alt' => 'GitHub',
					'url_by'  => 'https://github.com/butuzov',
					'desc'    => 'Adds information about Rewrite Rules (changed via filters) to a new panel in the Debug Bar.',
					'url_img' => 'https://ps.w.org/debug-bar-rewrite-rules/assets/icon-128x128.png?rev=1843137'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/debug-bar-rewrite-rules/',
					'url_repo' => 'https://github.com/butuzov/Debug-Bar-Rewrite-Rules'
				]
			]
		],

		'debug-bar-roles-and-capabilities' => [
			'name'     => $str_prefix . 'Debug Bar: Roles and Capabilities',
			'slug'     => 'debug-bar-roles-and-capabilities',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Matthew Boynes',
					'url_by'  => 'http://boyn.es/',
					'desc'    => 'Adds a “Roles and Capabilities” panel to Debug Bar that tabulates all the roles and capabilities on the site. This plugin requires the Debug Bar Plugin.',
					'url_img' => 'https://secure.gravatar.com/avatar/6b5e3a6a664bcb435216ecba76c9ed90?s=100&d=mm&r=g'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/debug-bar-roles-and-capabilities/',
				]
			]
		],

		'debug-bar-shortcodes' => [
			'name'     => $str_prefix . 'Debug Bar: Shortcodes',
			'slug'     => 'debug-bar-shortcodes',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Juliette Reinders Folmer',
					'by_alt'  => 'WP.org Profile',
					'url_by'  => 'https://profiles.wordpress.org/website-redirect/jrf',
					'desc'    => 'Adds a new panel to the Debug Bar that displays the registered shortcodes for the current request.',
					'url_img' => 'https://ps.w.org/debug-bar-shortcodes/assets/icon-128x128.png?rev=981236'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/debug-bar-shortcodes/',
				]
			]
		],

		'debug-bar-sidebars-widgets' => [
			'name'     => $str_prefix . 'Debug Bar: Sidebars & Widgets',
			'slug'     => 'debug-bar-sidebars-widgets',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Jesper van Engelen',
					'by_alt'  => 'WP.org Profile',
					'url_by'  => 'https://profiles.wordpress.org/engelen/',
					'desc'    => 'Shows you all you need to know about the sidebars and widgets on the current page.',
					'url_img' => 'https://secure.gravatar.com/avatar/6865eb7f453c78192d4ef5e26a94797a?s=100&d=mm&r=g'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/debug-bar-sidebars-widgets/',
				]
			]
		],


		'debug-bar-super-globals' => [
			'name'     => $str_prefix . 'Debug Bar: Super Globals',
			'slug'     => 'debug-bar-super-globals',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'horike takahiro',
					'by_alt'  => 'WP.org Profile',
					'url_by'  => 'https://profiles.wordpress.org/horike/',
					'desc'    => 'Displays Super Global variables for the current request.Like $_POST, $_GET, and $_COOKIE.',
					'url_img' => 'https://secure.gravatar.com/avatar/53c9bf20050e5457f054eac61c388a15?s=100&d=mm&r=g'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/debug-bar-super-globals/',
					'url_repo' => 'https://github.com/horike37/Debug-Bar-Super-Globals'
				]
			]
		],

		'debug-bar-taxonomies' => [
			'name'     => $str_prefix . 'Debug Bar: Taxonomies',
			'slug'     => 'debug-bar-taxonomies',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Juliette Reinders Folmer',
					'by_alt'  => 'WP.org Profile',
					'url_by'  => 'https://profiles.wordpress.org/website-redirect/jrf',
					'desc'    => 'Adds a new panel to the Debug Bar that displays detailed information about the registered taxonomies for your site.',
					'url_img' => 'https://ps.w.org/debug-bar-taxonomies/assets/icon-128x128.png?rev=1406128'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/debug-bar-taxonomies/',
				]
			]
		],


		'debug-bar-widgets' => [
			'name'     => $str_prefix . 'Debug Bar: Widgets',
			'slug'     => 'debug-bar-widgets',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Chris Hardie',
					'url_by'  => 'https://chrishardie.com/',
					'desc'    => 'Adds a new panel to the Debug Bar that displays all registered widgets.',
					'url_img' => 'https://ps.w.org/debug-bar-widgets/assets/icon-256x256.png?rev=1764336'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/debug-bar-widgets/',
					'url_repo' => 'https://github.com/ChrisHardie/debug-bar-widgets'
				]
			]
		],


		'woocommerce-debug-bar' => [
			'name'     => $str_prefix . 'Debug Bar: WooCommerce',
			'slug'     => 'woocommerce-debug-bar',
			'required' => false,
			'wpez'     => [
				'info'      => [
					'by'      => 'Coen Jacobs',
					'url_by'  => 'https://coenjacobs.me/',
					'desc'    => 'Adds a WooCommerce debug panel to the Debug Bar plugin. ',
					'url_img' => 'https://secure.gravatar.com/avatar/774dda6c8b4e37564e980522349bff83?s=100&d=mm&r=g'
				],
				'resources' => [
					'url_wp_org' => 'https://wordpress.org/plugins/woocommerce-debug-bar/',
					'url_repo' => 'https://github.com/coenjacobs/woocommerce-debug-bar'
				]
			]
		],

	];

	$arr_mod = apply_filters( __NAMESPACE__ . '\plugins', $arr );

	if ( ! is_array( $arr_mod ) ) {
		$arr_mod = $arr;
	}

	$arr_new = array_values( $arr_mod );

	if ( is_array( $arr_in ) ) {

		return array_merge( $arr_in, $arr_new );
	}

	return $arr_new;
}